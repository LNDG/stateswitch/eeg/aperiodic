%% setup 

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/';
    pn.tools = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
    addpath([pn.root, 'T_tools/brewermap']) % add colorbrewer
    addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar
    addpath([pn.root, 'T_tools/barwitherr/']); % add barwitherr
    addpath([pn.root, 'T_tools/']); % add convertPtoExponential
    
    addpath(genpath([pn.root, 'T_tools/RainCloudPlots/'])); % add barwitherr

%% load data

    pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
    load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

%% plot parametric 1/f slope modulation

    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
    
    summaryIdx = ismember(STSWD_summary.IDs, IDs);  
%     
%     slopeFit = STSWD_summary.spectralslope(summaryIdx,:);
% 
%     colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];
% 
%     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%     condPairsLevel = [-.027, -.028, -.029, -.0275, -.028, -.32];
% 
%     % plot within-subject standard errors
%     h = figure('units','normalized','position',[.1 .1 .2 .2]);
%         meanY = nanmean(slopeFit,1);    
%         condAvg = squeeze(nanmean(slopeFit,2));
%         curData = squeeze(slopeFit);
%         curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%         errorY = nanstd(curData,1)./sqrt(size(curData,1));
%         [h1, hError] = barwitherr(errorY, meanY);
%         for indPair = 1:size(condPairs,1)
%             % significance star for the difference
%             [~, pval] = ttest(slopeFit(:,condPairs(indPair,1)), slopeFit(:,condPairs(indPair,2))); % paired t-test
%             % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%             % sigstars on top
%             if pval <.05
%                 mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%             end
%         end
%         set(h1(1),'FaceColor',colorm(1,:));
%         set(h1(1),'LineWidth',2);
%         set(hError(1),'LineWidth',3);
%         box(gca,'off')
%         set(gca, 'XTick', [1,2,3,4]);
%         set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
%         xlabel('Target load'); ylabel({'PSD Slope'})
%         title('1/f slope modulation'); 
%         set(findall(gcf,'-property','FontSize'),'FontSize',30)
%         ylim([-.029 -.022]); xlim([.25 4.75]);
% 
% pn.plotFolder = [pn.root, 'C_figures/'];
% figureName = 'C_1_f_modulation';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

%% plot RainCloudPlot

% define outlier as lin. modulation of 2.5*mean Cook's distance
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/T_tools/Cookdist/'))
cooks = Cookdist(STSWD_summary.OneFslope.data(summaryIdx,:));
%outliers = cooks>2.5*mean(cooks);
outliers = cooks>4/numel(cooks);
find(outliers)

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i)-...
            nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2)+...
            repmat(nanmean(nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2),1),size(STSWD_summary.OneFslope.data(summaryIdx,:),1),1);
        
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}([19,27,37]) = [];
        
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}([19,27,37]) = [];
        
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}([1:18,20:26,28:36,38:end]); data_ws{i, j}([19,27,37])];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1);
    % add stats
%     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%     condPairsLevel = [-.0275, -.028, -.029, -.028, -.0283, -.029]+.006;
    condPairs = [3,4];
    condPairsLevel = [-.023];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data_nooutlier{condPairs(indPair,1), j}, data_nooutlier{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'1/f slope modulation'; 'linear effect: p = 7e-4'}); 
set(findall(gcf,'-property','FontSize'),'FontSize',30)
xlim([-.032 -.022]); yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_1_f_modulation_RCplot';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% remove only indicated outlier

% outliers = find(STSWD_summary.OneFslope.linear(summaryIdx)>mean(STSWD_summary.OneFslope.linear(summaryIdx)) + 3*std(STSWD_summary.OneFslope.linear(summaryIdx)) &...
%     STSWD_summary.OneFslope.linear(summaryIdx)>mean(STSWD_summary.OneFslope.linear(summaryIdx)) + 3*std(STSWD_summary.OneFslope.linear(summaryIdx)));
% 
% fitlm([1,1;1,2;1,3;1,4], STSWD_summary.OneFslope.data(2,:))

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = STSWD_summary.OneFslope.data(summaryIdx,i)-...
            nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2)+...
            repmat(nanmean(nanmean(STSWD_summary.OneFslope.data(summaryIdx,:),2),1),size(STSWD_summary.OneFslope.data(summaryIdx,:),1),1);
        
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}([27,37]) = [];
        
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}([27,37]) = [];
        
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}([1:26,28:36,38:end]); data_ws{i, j}([27,37])];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1);
    % add stats
%     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%     condPairsLevel = [-.0275, -.028, -.029, -.028, -.0283, -.029]+.006;
    condPairs = [3,4];
    condPairsLevel = [-.023];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data_nooutlier{condPairs(indPair,1), j}, data_nooutlier{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'PSD Slope'; '[Individually centered]'})

% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'1/f slope modulation'; 'linear effect: p = 3e-4'}); 
set(findall(gcf,'-property','FontSize'),'FontSize',30)
xlim([-.032 -.022]); yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_1_f_modulation_RCplot';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');