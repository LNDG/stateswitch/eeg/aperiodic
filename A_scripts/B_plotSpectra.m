%% set paths

restoredefaultpath;

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/';
pn.tools = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;
addpath([pn.root, 'T_tools/brewermap']) % add colorbrewer
addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar
addpath([pn.root, 'T_tools/barwitherr/']); % add barwitherr
addpath([pn.root, 'T_tools/']); % add convertPtoExponential

%% load PSD data

% N = 47 YA
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

MergedPow = [];
for id = 1:length(IDs)
    pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/A_1_f/';
    load([pn.dataOut, IDs{id}, '_1f_v3.mat'], 'freq');
    MergedPow(id,:,:,:) = freq.powspctrm;
end

%% linear fit between 1-64 Hz (excluding 8-15 Hz range, 28-38 Hz)
% shallower slope --> increasing neural irregularity

nonAlphaSub30 = find(freq.freq <8 | freq.freq >15 & freq.freq <28 | freq.freq >32);

linFit_2_30 = [];
for indID = 1:size(MergedPow,1)
    for indCond = 1:4
        for indChan = 1:60
            x = log10(freq.freq(nonAlphaSub30))';
            y = log10(squeeze(MergedPow(indID,indCond,indChan,nonAlphaSub30)));
            pv=polyfit(x,y,1); % linear regression
            linFit_2_30(indID, indCond, indChan) = pv(1);
        end
    end
end

SlopeFits.IDs = IDs;
SlopeFits.linFit_2_30 = linFit_2_30;

save([pn.root, 'B_data/C_SlopeFits_v3.mat'], 'SlopeFits')
load([pn.root, 'B_data/C_SlopeFits_v3.mat'], 'SlopeFits')

%% topography of slope fits

% 
% cfg = [];
% cfg.layout = 'acticap-64ch-standard2.mat';
% cfg.parameter = 'powspctrm';
% cfg.comment = 'no';
% cfg.colorbar = 'SouthOutside';
% cfg.zlim = [-.03 .03];
% 
% figure;
% subplot(2,2,1)
%     plotData = [];
%     plotData.label = freq.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(linFit_2_30(:,1,:),1));
%     ft_topoplotER(cfg,plotData);
%     title('Slopes EC, YA')
% subplot(2,2,2)
%     plotData = [];
%     plotData.label = freq.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(linFit_2_30(:,4,:),1));
%     ft_topoplotER(cfg,plotData);
%     title('Slopes EO, YA')
% subplot(2,2,3)
%     plotData = [];
%     plotData.label = freq.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(linFit_2_30(:,4,:),1))-squeeze(nanmean(linFit_2_30(:,1,:),1));
%     ft_topoplotER(cfg,plotData);
%     title('Slopes EC, OA')
% 
    
%% CBPA
    
    StatStruct = [];
    for indCond = 1:4
        StatStruct{indCond} = freq;
        StatStruct{indCond}.freq = 1;
        StatStruct{indCond}.powspctrm = squeeze(linFit_2_30(:,indCond,:));
        StatStruct{indCond}.dimord = 'subj_chan';
    end

    % prepare_neighbours determines what sensors may form clusters
    cfg_neighb.method       = 'template';
    cfg_neighb.template     = 'elec1010_neighb.mat';
    cfg_neighb.channel      = StatStruct{1}.label;

    cfgStat = [];
    cfgStat.channel          = 'all';
    cfgStat.method           = 'montecarlo';
    cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
    cfgStat.correctm         = 'cluster';
    cfgStat.clusteralpha     = 0.05;
    cfgStat.clusterstatistic = 'maxsum';
    %cfgStat.minnbchan        = 2;
    cfgStat.tail             = 0;
    cfgStat.clustertail      = 0;
    cfgStat.alpha            = 0.05;
    cfgStat.numrandomization = 1500;
    cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1,1,1});

    subj = size(StatStruct{indCond}.powspctrm,1);
    conds = 4;
    design = zeros(2,conds*subj);
    for indCond = 1:conds
    for i = 1:subj
        design(1,(indCond-1)*subj+i) = indCond;
        design(2,(indCond-1)*subj+i) = i;
    end
    end
    cfgStat.design   = design;
    cfgStat.ivar     = 1;
    cfgStat.uvar     = 2;

    stat = [];
    cfgStat.parameter = 'powspctrm';
    [stat] = ft_freqstatistics(cfgStat, StatStruct{1}, StatStruct{2}, StatStruct{3}, StatStruct{4});

    save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_stat_v3.mat', 'stat')
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_stat_v3.mat', 'stat')

    % constrain to occipital cluster
    stat.mask(stat.posclusterslabelmat~=2) = 0;
    
    % IMPORTANT: we do not report the effect over motor channels here to focus
    % on occipital activity
    
%% plot topography

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    %figure; imagesc(stat.stat.*stat.mask)

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = stat.label(stat.mask);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 40;
    cfg.zlim = [-5 5];
    cfg.style = 'both';
    cfg.colormap = cBrew;

    h = figure('units','normalized','position',[.1 .1 .2 .2]); 
    plotData = [];
    plotData.label = stat.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(stat.stat);
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t values');
    pval = []; pval = convertPtoExponential(stat.posclusters(2).prob);
    title({'Linear effect 1/f slopes', ['p = ', pval{1}]});
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_1_f_topography_v3';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot 1/f rotation

    % select only posterior-occipital channels
    stat.posclusterslabelmat(1:43) = 0;

    % shading presents within-subject standard errors
    % new value = old value ??? subject average + grand average

    cBrew = brewermap(4,'RdBu');
    cBrew = flipud(cBrew);
    h = figure('units','normalized','position',[.1 .1 .2 .2]); hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
    xlim([0 1.9]); ylim([-10 -6.5])
    xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
    legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthEast'); legend('boxoff')
    title('Divided attention induces 1/f rotation')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

    % add emphasis of effect ranges
    
    handaxes1 = axes('Position', [0.22 0.3 0.2 .2]); cla;
    hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
    xlim([0 .1]); ylim([-7.3 -7.1]) % ylim([-7.05 -6.85])
    set(findall(handaxes1,'-property','FontSize'),'FontSize',14)

    handaxes2 = axes('Position', [0.68 0.65 0.2 .2]); cla;
    hold on;
    condAvg = squeeze(nanmean(nanmean(log10(MergedPow(:,:,find(stat.posclusterslabelmat),:)),3),2));
    curData = squeeze(nanmean(log10(MergedPow(:,1,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
    curData = squeeze(nanmean(log10(MergedPow(:,4,find(stat.posclusterslabelmat),:)),3));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
        'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
    xlim([1.75 1.9]); ylim([-9.15 -8.88])
    set(findall(handaxes2,'-property','FontSize'),'FontSize',14)

pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'C_1_f_rotation_v3';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot parametric effect

%     slopeFit = nanmean(linFit_2_30(:,:,find(stat.posclusterslabelmat)),3);
% 
%     colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];
% 
%     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%     condPairsLevel = [-.027, -.028, -.029, -.0275, -.028, -.32];
% 
%     % plot within-subject standard errors
%     h = figure('units','normalized','position',[.1 .1 .2 .2]);
%         meanY = nanmean(slopeFit,1);    
%         condAvg = squeeze(nanmean(slopeFit,2));
%         curData = squeeze(slopeFit);
%         curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%         errorY = nanstd(curData,1)./sqrt(size(curData,1));
%         [h1, hError] = barwitherr(errorY, meanY);
%         for indPair = 1:size(condPairs,1)
%             % significance star for the difference
%             [~, pval] = ttest(slopeFit(:,condPairs(indPair,1)), slopeFit(:,condPairs(indPair,2))); % paired t-test
%             % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
%             % sigstars on top
%             if pval <.05
%                 mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
%             end
%         end
%         set(h1(1),'FaceColor',colorm(1,:));
%         set(h1(1),'LineWidth',2);
%         set(hError(1),'LineWidth',3);
%         box(gca,'off')
%         set(gca, 'XTick', [1,2,3,4]);
%         set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
%         xlabel('Target load'); ylabel({'PSD Slope'})
%         title('1/f slope modulation'); 
%         set(findall(gcf,'-property','FontSize'),'FontSize',30)
%         ylim([-.029 -.022]); xlim([.25 4.75]);
% 
% pn.plotFolder = [pn.root, 'C_figures/'];
% figureName = 'C_1_f_modulation';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');
% 
% %% relation to AMF 
% 
%     load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')
% 
%     jointsubs = intersect(EEGAttentionFactor.IDs,IDs);
%     attFctrIdx = find(ismember(EEGAttentionFactor.IDs,jointsubs));
%     spectraIdx = find(ismember(IDs,jointsubs));
% 
%     % correlation with 234-1 difference
%     
%     SlopeEffect = squeeze(nanmean(nanmean(linFit_2_30(spectraIdx, 2:4, find(stat.posclusterslabelmat)),2)-...
%         linFit_2_30(spectraIdx, 1, find(stat.posclusterslabelmat)),3));
%     
%     figure;
%     scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), SlopeEffect)
%     [r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), SlopeEffect);
%     disp(['Correlation of PSD slope modulation with AMF: r = ', num2str(r(2)), '; p = ', num2str(p(2))]);
%     
%     % plot median split results
%     
%     [sortVal, sortIdx] = sort(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), 'descend');
% 
%     highChange = sortIdx(1:ceil(numel(sortIdx)/2));
%     lowChange = sortIdx(ceil(numel(sortIdx)/2)+1:end);
% 
%     h = figure('units','normalized','position',[.1 .1 .4 .4]); hold on;
%     subplot(2,2,1); hold on;
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(highChange,1,find(stat.posclusterslabelmat),:)),3),1)), 'LineWidth', 1)
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(highChange,4,find(stat.posclusterslabelmat),:)),3),1)), 'LineWidth', 1)
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('High AMF');
%     subplot(2,2,2); hold on;
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(lowChange,1,find(stat.posclusterslabelmat),:)),3),1)), 'LineWidth', 1)
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(lowChange,4,find(stat.posclusterslabelmat),:)),3),1)), 'LineWidth', 1)
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('Low AMF');
% 
%     %h = figure('units','normalized','position',[.1 .1 .2 .4]); hold on;
%     subplot(2,2,3); hold on;
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(highChange,1,find(stat.posclusterslabelmat),:)),3),1)-...
%             repmat(nanmean(nanmean(nanmean(log10(MergedPow(highChange,[1,4],find(stat.posclusterslabelmat),:)),3),1),2),1,4,1,1)), 'LineWidth', 1)
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(highChange,4,find(stat.posclusterslabelmat),:)),3),1)-...
%             repmat(nanmean(nanmean(nanmean(log10(MergedPow(highChange,[1,4],find(stat.posclusterslabelmat),:)),3),1),2),1,4,1,1)), 'LineWidth', 1)
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('High AMF');
%     subplot(2,2,4); hold on;
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(lowChange,1,find(stat.posclusterslabelmat),:)),3),1)-...
%             repmat(nanmean(nanmean(nanmean(log10(MergedPow(highChange,[1,4],find(stat.posclusterslabelmat),:)),3),1),2),1,4,1,1)), 'LineWidth', 1)
%         plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(MergedPow(lowChange,4,find(stat.posclusterslabelmat),:)),3),1)-...
%             repmat(nanmean(nanmean(nanmean(log10(MergedPow(highChange,[1,4],find(stat.posclusterslabelmat),:)),3),1),2),1,4,1,1)), 'LineWidth', 1)
%         set(findall(gcf,'-property','FontSize'),'FontSize',18)
%         title('Low AMF'); 
% 
% %% Plot lack of rotation for low AMF subjects:
% 
% % shading presents within-subject standard errors
%     % new value = old value ??? subject average + grand average
% 
%     cBrew = brewermap(4,'RdBu');
%     h = figure('units','normalized','position',[.1 .1 .2 .2]); hold on;
%     condAvg = squeeze(nanmean(nanmean(log10(MergedPow(lowChange,:,find(stat.posclusterslabelmat),:)),3),2));
%     curData = squeeze(nanmean(log10(MergedPow(lowChange,1,find(stat.posclusterslabelmat),:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .25);
%     curData = squeeze(nanmean(log10(MergedPow(lowChange,4,find(stat.posclusterslabelmat),:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .25);
%     xlim([-.3 1.75]); ylim([-10 -6.5])
%     xlabel('Frequency (log10)'); ylabel('Power (log10; Hz)');
%     legend([l1.mainLine, l4.mainLine], {'1 Target', '4 Targets'}, 'location', 'SouthEast'); legend('boxoff')
%     title('Divided attention induces 1/f rotation')
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
%     % add emphasis of effect ranges
%     
%     handaxes1 = axes('Position', [0.22 0.3 0.2 .2]); cla;
%     hold on;
%     condAvg = squeeze(nanmean(nanmean(log10(MergedPow(lowChange,:,find(stat.posclusterslabelmat),:)),3),2));
%     curData = squeeze(nanmean(log10(MergedPow(lowChange,1,find(stat.posclusterslabelmat),:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
%     curData = squeeze(nanmean(log10(MergedPow(lowChange,4,find(stat.posclusterslabelmat),:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
%     xlim([-.3 -.2]); ylim([-7.05 -6.85])
%     set(findall(handaxes1,'-property','FontSize'),'FontSize',14)
% 
%     handaxes2 = axes('Position', [0.68 0.65 0.2 .2]); cla;
%     hold on;
%     condAvg = squeeze(nanmean(nanmean(log10(MergedPow(lowChange,:,find(stat.posclusterslabelmat),:)),3),2));
%     curData = squeeze(nanmean(log10(MergedPow(lowChange,1,find(stat.posclusterslabelmat),:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l1 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(1,:),'linewidth', 1}, 'patchSaturation', .25);
%     curData = squeeze(nanmean(log10(MergedPow(lowChange,4,find(stat.posclusterslabelmat),:)),3));
%     curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
%     standError = nanstd(curData,1)./sqrt(size(curData,1));
%     l4 = shadedErrorBar(log10(freq.freq),nanmean(curData,1),standError, ...
%         'lineprops', {'color', cBrew(4,:),'linewidth', 1}, 'patchSaturation', .25);
%     xlim([1.6 1.75]); ylim([-9.3 -9.1])
%     set(findall(handaxes2,'-property','FontSize'),'FontSize',14)
