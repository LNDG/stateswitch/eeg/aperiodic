% Calculate 1/f spectra for StateSwitch Dynamic

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
pn.FieldTrip    = [pn.root, 'B_analyses/S2_TFR/T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)

    display(['processing ID ' IDs{id}]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

     %% CSD transform
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo; clear TrlInfo;
    data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;
  
    %% calculate FFT

    for indCond = 1:4
        cfg = [];
        cfg.trials = find(data.TrlInfo(:,8)==indCond);
        cfg.toilim = [3.49 6.2];
        curData = ft_redefinetrial(cfg, data);

        curData = rmfield(curData, 'TrlInfo');
        curData = rmfield(curData, 'TrlInfoLabels');

        param.FFT.trial       = 'all';
        param.FFT.method      = 'mtmconvol';       % analyze entire spectrum; multitaper
        param.FFT.output      = 'pow';          % only get power values
        param.FFT.keeptrials  = 'no';           % return individual trials
        param.FFT.taper       = 'hanning';      % use hanning windows
        param.FFT.tapsmofrq   = 2;
        param.FFT.foi         = 2.^[0:.125:6.5];  % frequencies of interest
        param.FFT.t_ftimwin   = repmat(2.5,1,numel(param.FFT.foi));
        param.FFT.toi         = 4.75;
        param.FFT.pad         = 10;
                
        tmpFreq = ft_freqanalysis(param.FFT, curData);
        
%         figure; plot(log10(param.FFT.foi), log10(squeeze(nanmean(tmpFreq.powspctrm(55:60,:),1))))
        
        if indCond == 1
            freq = tmpFreq;
            freq.powspctrm = [];
            freq.powspctrm(1,:,:) = tmpFreq.powspctrm;
        else
            freq.powspctrm(indCond,:,:) = tmpFreq.powspctrm;
        end
        clear tmpFreq;
        
    end

    %% save output
    
    pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/A_1_f/';
    save([pn.dataOut, IDs{id}, '_1f_v3.mat'], 'freq');
        
end % ID